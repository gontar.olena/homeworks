import React, {Component} from 'react';


// class Header extends Component{
 
//   render(){
//     const currentDate  = new Date();
//     let day = currentDate.getDate();
//     let month = currentDate.getMonth();
//     let weekday = currentDate.getDay();
    
//     const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
//     const months=['January','February','March', 'April','May','June','July','August','September','October','November','December']
//     return(
//       <div className="header">
//         <div className="current-date">
//           <p className="day">{weekdays[weekday]}</p>
//           <p className="data">{day} {months[month]}</p>
//         </div>
//       </div>
//     )
//   }
// }

const Header = () => {
  const currentDate  = new Date();
    let day = currentDate.getDate();
    let month = currentDate.getMonth();
    let weekday = currentDate.getDay();
    const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const months=['January','February','March', 'April','May','June','July','August','September','October','November','December'];

    return(
            <div className="header">
              <div className="current-date">
                <p className="day">{weekdays[weekday]}</p>
                <p className="data">{day} {months[month]}</p>
              </div>
            </div>
          )
}

export default Header;