import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './Button.scss';

// class Button extends Component{
       
//     render(){
//         const {textBtn, onClick} = this.props;
//         console.log('Button', this.props);
//         return (
//         <button className="button" type="button" onClick = {onClick}>{textBtn}</button>)        
//     }
// }

const Button = ({children, onClick, ...restProps}) => {
    return (
         <button className="button" type="button" onClick = {onClick} {...restProps}>{children}</button>)        
}

Button.propTypes={
    children: PropTypes.string,
    onClick: PropTypes.func
}


export default Button;