import React, {Component} from 'react';
import PropTypes from 'prop-types'
import Card from './Card';
import './Cards.scss';

// class Cards extends Component{
    
//     render(){
//         const {forecastProps} = this.props;
//         console.log('Cards',forecastProps);
//         const forecastCards = forecastProps.map((card, index) => <Card key={index} cardProps={card} />)

//         return(
//             <div className="forecast">           
//             {forecastCards}            
//             </div>
//         )
//     }
// }
const Cards = ({forecastProps}) => {
    const forecastCards = forecastProps.map((card, index) => <Card key={index} cardProps={card} />)

             return(
                 <div className="forecast">           
                 {forecastCards}            
                 </div>
             )
}

Cards.propTypes = {
    cardProps: PropTypes.array

}
export default Cards;