import React, {Component} from 'react';
import PropTypes from 'prop-types';

// class Card extends Component{
    
//     render(){
//         const {cardProps} = this.props;
//         const {weekDay, month, day, forecastIcon, forecastText,maxTemp} = cardProps;
//         const currentDay = new Date();
//         const isSelected = day === currentDay.getDate() ?"forecast__card card--selected":"forecast__card"

//         return(
            
//             <div className={isSelected}>
//                 <p className="card__data">{weekDay} <br/> {day} {month}</p>
//                 <div className="card__icon">
//                     <img src={forecastIcon} alt={forecastText} />
//                 </div>
//                 <span className="card__temp">{Math.round(maxTemp)}</span>
//             </div>
           
            
       
//         )
//     }
// }

const Card =({cardProps}) => {
    const {weekDay, month, day, forecastIcon, forecastText,maxTemp} = cardProps;
    const currentDay = new Date();
    const isSelected = day === currentDay.getDate() ?"forecast__card card--selected":"forecast__card"

    return(        
        <div className={isSelected}>
            <p className="card__data">{weekDay} <br/> {day} {month}</p>
            <div className="card__icon">
                <img src={forecastIcon} alt={forecastText} />
            </div>
            <span className="card__temp">{Math.round(maxTemp)}</span>
        </div>        
   )
}

Card.propTypes = {
    cardProps: PropTypes.object.isRequired

}
export default Card;