import React, {useState} from 'react';
import Meta from './components/Meta';
import Cards from './components/Cards';
import { sendRequest } from '../../helpers';
import {API_URL, API_KEY} from '../../config/Api';
import { useEffect } from 'react';
const CITY= "kiev";
const DAYS=5;

// class Forecast extends Component {
// // state ={
// //     current:{
// //         cloud: 46,
// //         humidity: 46,
// //         temp: 20,
// //     },
// //     forecast: [
// //         {
// //             weekDay: 'Wednesday',
// //             month: 'August',
// //             day: 31,
// //             forecastIcon: 'http://cdn.weatherapi.com/weather/64x64/day/116.png',
// //             forecastText: 'Partly cloudy',
// //             maxTemp: 23.2,
// //         },
// //         {
// //             weekDay: 'Thursday',
// //             month: 'September',
// //             day: 1,
// //             forecastIcon: 'http://cdn.weatherapi.com/weather/64x64/day/116.png',
// //             forecastText: 'Partly cloudy',
// //             maxTemp: 21.5,
// //         },
// //         {
// //             weekDay: 'Wednesday',
// //             month: 'August',
// //             day: 31,
// //             forecastIcon: 'http://cdn.weatherapi.com/weather/64x64/day/116.png',
// //             forecastText: 'Partly cloudy',
// //             maxTemp: 23.2,
// //         },
// //         {
// //             weekDay: 'Wednesday',
// //             month: 'August',
// //             day: 31,
// //             forecastIcon: 'http://cdn.weatherapi.com/weather/64x64/day/116.png',
// //             forecastText: 'Partly cloudy',
// //             maxTemp: 23.2,
// //         },
// //         {
// //             weekDay: 'Wednesday',
// //             month: 'August',
// //             day: 31,
// //             forecastIcon: 'http://cdn.weatherapi.com/weather/64x64/day/116.png',
// //             forecastText: 'Partly cloudy',
// //             maxTemp: 23.2,
// //         },
// //     ]
// // }
// state = {
//     current: {},
//     location: "",
//     forecast: []
// }

// componentDidMount(){
//     sendRequest(`${API_URL}?key=${API_KEY}&q=${CITY}&days=${DAYS}&aqi=no&alerts=no`)
//         .then(({current, location, forecast}) => {
//             const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
//             const months=['January','February','March', 'April','May','June','July','August','September','October','November','December']

//             const new_forecast = forecast.forecastday.map((item)=>{
//                 const currentDate = new Date(item.date);
               
//                 let day = currentDate.getDate();
//                 let month = currentDate.getMonth();
//                 let weekday = currentDate.getDay();
                
//                 return {
//                     weekDay: weekdays[weekday],
//                     month: months[month],
//                     day: day,
//                     forecastIcon: item.day.condition.icon,
//                     forecastText: item.day.condition.text,
//                     maxTemp: item.day.maxtemp_c,
//                 }
//             })
            
//             this.setState({
//                 current:{
//                            cloud: current.cloud,
//                            humidity: current.humidity,
//                            temp: current.temp_c,
//                         },
//                 location: location.name,
//                 forecast: new_forecast
                
//             })
//         }
//     )
// }
//     render(){
//         const  {current, forecast, location} = this.state;
//         console.log(current)
//         return(
//             <>
//             <Meta currentProps = {current} city = {location} />
//             <Cards forecastProps = {forecast} />
//             </>
//         )
//     }
// }

const Forecast = () => {
    const [current, setCurrent] = useState({});
    const [location, setLocation] = useState("");
    const [forecast, setForecast] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [city, setCity] = useState('kiev')

    setTimeout(()=>{
        setCity("London")
     }, 3000)

    useEffect(
        ()=> {
        sendRequest(`${API_URL}?key=${API_KEY}&q=${city}&days=${DAYS}&aqi=no&alerts=no`)
         .then(({current, location, forecast}) => {
        const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        const months=['January','February','March', 'April','May','June','July','August','September','October','November','December']

        const new_forecast = forecast.forecastday.map((item)=>{
            const currentDate = new Date(item.date);
           
            let day = currentDate.getDate();
            let month = currentDate.getMonth();
            let weekday = currentDate.getDay();
            
            return {
                weekDay: weekdays[weekday],
                month: months[month],
                day: day,
                forecastIcon: item.day.condition.icon,
                forecastText: item.day.condition.text,
                maxTemp: item.day.maxtemp_c,
            }
        })
        console.log("location", location.name)
        setCurrent({
            cloud: current.cloud,
            humidity: current.humidity,
            temp: current.temp_c,
         })
         setLocation(location.name)  
         setForecast(new_forecast)  
         setTimeout(()=>{
            setIsLoading(false)
         }, 2000)
                     
        })
    }, [city])
    
            
        

    return(       
            <>
            <Meta currentProps = {current} city = {location} />
            <Cards forecastProps = {forecast} />
            </>
    )
}

export default Forecast;