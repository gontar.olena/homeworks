import React, {Component} from 'react';
import Button from "../Button";
import PropTypes from 'prop-types';

// class Controllers extends Component{
//     render(){
//         const {increase, decrease} = this.props;
//         return(
//         <div className="widget-controllers">
//             <div className="button-container">
//                 <Button children='Cold' onClick = {decrease}  />
//                 <Button children= 'Hot' onClick = {increase}  />
//             </div>
//         </div>
//         )
//     }
// }

const Controllers =({increase, decrease}) => {
    return(
                <div className="widget-controllers">
                    <div className="button-container">
                        <Button children='Cold' onClick = {decrease}  />
                        <Button children= 'Hot' onClick = {increase}  />
                    </div>
                </div>
                )
}

Controllers.propTypes={
    increase: PropTypes.func,
    decrease: PropTypes.func
}

export default Controllers;