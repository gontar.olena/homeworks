import React, {useState} from 'react';
import Controllers from './components/Controllers';
import Forecast from './components/Forecast';
import Header from './components/Header';
import Temperature from './components/Temperature';
import './temperature.scss';

// class App extends Component{
//   state = {
//     temperature: 20,
//   }
//   increase = () => {
//     const {temperature} = this.state;
//     if(temperature >= 29) return;
//     this.setState({temperature:this.state.temperature + 1});
//   } 
//   decrease= () => {
//     const {temperature} = this.state;
//     if(temperature <= 0) return;
//     this.setState({temperature:this.state.temperature - 1});
//   } 

//   displayBg = () => {
//     const {temperature} = this.state;
//     if(temperature <= 10) return 'widget-container cold'
//     if(temperature <= 20) return 'widget-container neutral'
//     return 'widget-container hot'
//   }

//   render(){
//     const {temperature} = this.state;
//     const classes = this.displayBg();

//     return(
//       <div className={classes}>
//         <Header />
//         <Temperature temperature = {temperature}/>
//         <Controllers increase = {this.increase} decrease = {this.decrease} />
//         <Forecast />
//       </div>
//     )
//   }
// }

const App = () => {
  const [temperature, setTemperature] = useState(20)
  const increase = () => {        
        if(temperature >= 29) return;
        setTemperature(temperature + 1);
      } 
  const decrease= () => {       
        if(temperature <= 0) return;
        setTemperature(temperature - 1);
      } 
  const displayBg = () => {
    if(temperature <= 10) return 'widget-container cold'
    if(temperature <= 20) return 'widget-container neutral'
    return 'widget-container hot'
  }
  const classes = displayBg();
  return(
          <div className={classes}>
             <Header />
             <Temperature temperature = {temperature}/>
             <Controllers increase = {increase} decrease = {decrease} />
             <Forecast />
           </div>
         )
}
export default App;
