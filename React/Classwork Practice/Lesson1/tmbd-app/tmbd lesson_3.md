# Завдання
Сделать helpers функцию sendRequest для работы с API themoviedb.org
API_URL  "https://api.themoviedb.org/3";
IMG_URL  "https://image.tmdb.org/t/p/w500";
API_KEY_3  "3f4ca4f3a9750da53450646ced312397";

Сделать компонет Movies

```html
<div class="films__title">Популярное По TV</div>
<div class="films__slider">
    <Swiper
        slidesPerView={5}
        spaceBetween={16}
        class="films__wrapper"
        navigation={true}
        grabCursor={false}
        draggable={false}
        preventClicksPropagation={true}
        preventClicks={true}
        scrollbar={{draggable: false, hide: true}}
        slideToClickedSlide={false}
        pagination={{clickable: true}}
    >
        <SwiperSlide class="film__item">
            <div class="film-poster">
                <img src="IMG_URL poster_path" alt='title'/>
            </div>
        </SwiperSlide>
    </Swiper>
</div>
<div class="films__title">Популярное В кинотеатрах</div>
<div class="films__slider">
    <Swiper
        slidesPerView={5}
        spaceBetween={16}
        class="films__wrapper"
        navigation={true}
        grabCursor={false}
        draggable={false}
        preventClicksPropagation={true}
        preventClicks={true}
        scrollbar={{draggable: false, hide: true}}
        slideToClickedSlide={false}
        pagination={{clickable: true}}
    >
        <SwiperSlide class="film__item">
            <div class="film-poster">
                <img src="IMG_URL poster_path" alt='title'/>
            </div>
        </SwiperSlide>
    </Swiper>
</div>

```

## Технічні вимоги
Для комонента Movies нужно будет использовать пакет swiper
import {Swiper, SwiperSlide} from 'swiper/react';

и стили
import 'swiper/css';
```scss
.films__title {
	position: relative;
	display: inline;
	margin: 0 26px;
	font-size: 32px;
	font-weight: 600;
	
	&:before {
		content: '';
		display: block;
		position: absolute;
		bottom: -12px;
		left: 0;
		width: 16%;
		height: 6px;
		border-radius: 6px;
		background-color: #afaeae;
	}
}

.films__wrapper {
	display: flex;
	flex-wrap: wrap;
	padding: 50px 0;
	
	&.pending {
		flex-wrap: nowrap;
		padding: 40px 26px;
		box-sizing: border-box;
		
		.film__item {
			display: flex;
			justify-content: center;
			align-items: center;
			min-height: 594px;
			border-radius: 4px;
			background-color: #dbdbdb;
			
			svg {
				width: 220px;
			}
			
			&:hover {
				transform: scale(1.1);
				z-index: 10;
			}
		}
	}
	
	.swiper-wrapper {
		padding: 40px 26px;
		box-sizing: border-box;
	}
	
	.film__item {
		flex-shrink: 0;
		box-sizing: border-box;
		width: 20%;
		margin-right: 16px;
		cursor: pointer;
		transition: all 450ms;
		
		&:hover {
			margin-right: 36px !important;
			
			.film-poster img {
				transform: scale(1.1);
				z-index: 10;
			}
		}
		
		.film-poster {
			img {
				position: relative;
				border-radius: 4px;
				height: 100%;
				width: 100%;
				object-fit: contain;
				transition: transform 0.25s;
			}
		}
	}
}

.films__slider {
	overflow: hidden;
}
```

Описати propTypes для Movies

Для отримання даних використати функцію:
```js
export const sendRequest = async (url) => {
	const response = await fetch(url);
	const result = await response.json();
	return result;
}
```