import { Swiper, SwiperSlide } from 'swiper/react';
import { sendRequest } from '../../../../helpers/sendRequest';
import { useState, useEffect } from 'react';
import { API_URL, IMG_URL, API_KEY_3 } from '../../../../configs/API';
import { Link } from 'react-router-dom';

import 'swiper/css';


const TV = () => {
    const [moviesTV, setMoviesTV] = useState([])
    useEffect(() => {
        sendRequest(`${API_URL}/discover/tv?api_key=${API_KEY_3}`)
            .then(({ results }) => setMoviesTV(results))
    }, [])
    const moviesRender = moviesTV.map(({ original_name, poster_path, id, overview, name }) => {
        return (
            <SwiperSlide key={id} className="film__item">
                <div className="film-poster">
                    <img src={`${IMG_URL}${poster_path}`} alt={original_name} />                    
                    <div className="film-poster-back">
                        <h1 className="film-poster__title">{name}</h1>
                        <p className="film-poster__subtitle"><i>{original_name}</i></p>
                        <p className="film-poster__desc">{overview}</p>
                        <Link to={`/tv/${id}`}>
                            More
                        </Link>
                    </div>
                </div>
            </SwiperSlide>)
    })

    return (
        <>
            <div className="films__title">Популярне По TV</div>
            <div className="films__slider">
                <Swiper
                    slidesPerView={5}
                    spaceBetween={16}
                    className="films__wrapper"
                    navigation={true}
                    grabCursor={false}
                    draggable={false}
                    preventClicksPropagation={true}
                    preventClicks={true}
                    scrollbar={{ draggable: false, hide: true }}
                    slideToClickedSlide={false}
                    pagination={{ clickable: true }}
                >
                    {moviesRender}
                </Swiper>
            </div>
        </>
    )
}


export default TV;