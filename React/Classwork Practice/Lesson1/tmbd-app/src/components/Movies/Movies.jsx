import TV from './components/TV/TV'
import Cinema from './components/Cinema/Cinema'
import './Movies.scss'
const Movies =() => {
    return(
        <>
        <TV />
        <Cinema /> 
        </>
        )
}

export default Movies;