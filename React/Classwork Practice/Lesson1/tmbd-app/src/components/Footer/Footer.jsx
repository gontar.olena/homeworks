import {Component} from 'react'

import './Footer.scss'

// class Footer extends Component {
//     render() {
//         return (
//             <footer className="g-footer">
//                 <div className="copywriting">Footer (c) 2022</div>
//             </footer>
//         )
//     }
// }

const Footer = () => {
    return (
                    <footer className="g-footer">
                        <div className="copywriting">Footer (c) 2022</div>
                    </footer>
                )
}
export default Footer
