import { useParams, Link, useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';


import { sendRequest } from '../../helpers/sendRequest';
import { API_URL, API_KEY_3, IMG_URL } from '../../configs/API';

import './TVPage.scss'

import {ReactComponent as RightArrow} from './images/right-arrow.svg' 



const TVPage = () => {
    
  
    const navigate = useNavigate();
    const goBack =() =>{
        navigate(-1);       
    }

    const { tv_id } = useParams();
    const [tv, setTv] = useState({});
    const [seasons, setSeasons] = useState([]);

    useEffect(()=>{
        sendRequest(`${API_URL}/tv/${tv_id}?api_key=${API_KEY_3}`)
        .then((data) => {
            const {backdrop_path,
                poster_path,
                name,
                original_name,
                genres,
                episode_run_time,
                overview,
                created_by,
                last_episode_to_air,
                next_episode_to_air,
                seasons} = data;
                
            setTv({backdrop_path,
            poster_path,
            name,
            original_name,
            genres,
            episode_run_time,
            overview,
            created_by,
            last_episode_to_air,
            next_episode_to_air});
            
            setSeasons(seasons)}
            )            
     },[])
    
     const {backdrop_path,
        poster_path,
        name,
        original_name,
        genres,
        episode_run_time,
        overview,
        created_by,
        last_episode_to_air,
        next_episode_to_air
        } = tv;
console.log(last_episode_to_air);
    const seasonRender = seasons.map(({air_date, episode_count, name, poster_path})=>{
        return (
            <div className="cards-seasons">
                        <div className="card">
                            <div className="card-post"><img src={`${IMG_URL}${poster_path}`} alt={name} /></div>
                            <div className="card-content">
                                <div className="card-name">{name}</div>
                                <div className="card-info">
                                    <span className="card-date">{air_date}</span>  |  <span className="card-episode">{episode_count} episodes</span>
                                </div>
                            </div>
                        </div>
                    </div>
        )
    })    
    return (
        <>
            <header className="page-movie-header">
                <div className="header-bg" style={{backgroundImage: `url(${IMG_URL}${backdrop_path})`}} />
                <div className="container">
                    <div className="header-wrapper">
                        <Link className="btn-back" onClick={goBack}><RightArrow /></Link>
                        <div className="header-poster">
                            <img src={`${IMG_URL}${poster_path}`} alt={name} />
                        </div>
                        <div className="header-content">
                            <p className="movie-name">{name}</p>
                            <p className="movie-subname"><i>{original_name}</i></p>
                            <p className="movie-info">
                                <span className="genres">
                                    <span>{name}</span>
                                </span>
                                <span className="runtime">{episode_run_time}</span>
                            </p>
                            <p className="movie-overview">
                                {overview}
                            </p>
                            <p className="movie-created">
                                <span>{name} <br /> Creator</span>
                            </p>
                        </div>
                    </div>
                </div>
            </header>
            <div className="page-movie-content">
                <div className="container">
                    <p className="content-title">Seasons</p>
                    {seasonRender}
                   
                    <p className="content-title">Episodes</p>
                    <div className="cards-episodes">
                        <div className="episode-item episodes-last">
                           {last_episode_to_air && (<div className="episode-wrapper">
                                <div className="episodes-post">
                                    <img src={`${IMG_URL}${last_episode_to_air.still_path}`} alt={last_episode_to_air.name} />
                                </div>
                                <div className="episodes-content">
                                    <p className="episodes-name">Episodes name: {last_episode_to_air.name}</p>
                                    <p className="episodes-date"><i>Episodes date: {last_episode_to_air.air_date}</i></p>
                                    <p className="episodes-info">Season: {last_episode_to_air.season_number} Episode: {last_episode_to_air.episode_number} Runtime: {last_episode_to_air.runtime}</p>
                                    <p className="episodes-overview">{last_episode_to_air.overview}</p>
                                </div>
                            </div>)}
                        </div>
                        <div className="episode-item episodes-next">
                          { next_episode_to_air && ( <div className="episode-wrapper">
                                <div className="episodes-post">
                                    <img src={`${IMG_URL}${next_episode_to_air.still_path}`} alt={next_episode_to_air.name} />
                                </div>
                                <div className="episodes-content">
                                    <p className="episodes-name">Episodes name: {next_episode_to_air.name}</p>
                                    <p className="episodes-date"><i>Episodes date: {next_episode_to_air.air_date}</i></p>
                                    <p className="episodes-info">Season: {next_episode_to_air.season_number} Episode: {next_episode_to_air.episode_number} Runtime: {next_episode_to_air.runtime}</p>
                                    <p className="episodes-overview">{next_episode_to_air.overview}</p>
                                </div>
                            </div>)}
                        </div>
                    </div>
                </div>
            </div>
            </> 
    )
}

export default TVPage