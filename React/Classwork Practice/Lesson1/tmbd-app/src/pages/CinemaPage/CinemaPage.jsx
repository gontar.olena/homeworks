import { useParams, Link, useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';


import { sendRequest } from '../../helpers/sendRequest';
import { API_URL, API_KEY_3, IMG_URL } from '../../configs/API';

import './CinemaPage.scss'

import { ReactComponent as RightArrow } from './images/right-arrow.svg'

const CinemaPage = () => {
    const navigation = useNavigate();
    const goBack = () => navigation(-1);

    const { movie_id } = useParams();
    const [movie, setMovie] = useState({});

    useEffect(() => {
        sendRequest(`${API_URL}/movie/${movie_id}?api_key=${API_KEY_3}`)
            .then((data) => {
                const { backdrop_path,
                    poster_path,
                    original_title,
                    title,
                    genres,
                    runtime,
                    overview,
                    release_date } = data;

                setMovie({
                    backdrop_path,
                    poster_path,
                    original_title,
                    title,
                    genres,
                    runtime,
                    overview,
                    release_date
                });

            })
    }, [])

    const { backdrop_path,
        poster_path,
        original_title,
        title,
        genres,
        runtime,
        overview,
        release_date
    } = movie;

    const genresRender = genres?.map(({name}, id) => <span key={id}>{name}</span>)
    return (
        <>
            <header className="page-movie-header">
                <div className="header-bg" style={{ backgroundImage: `url(${IMG_URL}${backdrop_path})` }} />
                <div className="container">
                    <div className="header-wrapper">
                        <Link className="btn-back" onClick={goBack}><RightArrow /></Link>
                        <div className="header-poster">
                            <img src={`${IMG_URL}${poster_path}`} alt={original_title} />
                        </div>
                        <div className="header-content">
                            <p className="movie-name">{title}</p>
                            <p className="movie-subname"><i>{original_title}</i></p>
                            <p className="movie-info">
                                <span className="genres">
                                    {genresRender}
                                </span>
                                <span className="runtime">{runtime}</span>
                            </p>
                            <p className="movie-overview">
                                {overview}
                            </p>
                            <p className="movie-release">
                                Release date: {release_date}
                            </p>
                        </div>
                    </div>
                </div>
            </header>

        </>)
}


export default CinemaPage