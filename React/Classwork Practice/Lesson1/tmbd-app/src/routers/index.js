import { Routes, Route } from 'react-router-dom';
import Movies from '../components/Movies/Movies';
import CinemaPage from '../pages/CinemaPage/CinemaPage';
import TVPage from '../pages/TVPage/TVPage';

const RootRouter = () => {
   return( <Routes>
        <Route path='/' element={<Movies />} />
        <Route path='/movie/:movie_id' element={<CinemaPage />} />
        <Route path='/tv/:tv_id' element={<TVPage />} />
        <Route path='*' element={<p>Упс, щось пішло не так</p>} />
    </Routes>
)} 

export default RootRouter