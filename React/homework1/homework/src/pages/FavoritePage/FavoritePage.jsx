
import { useSelector } from 'react-redux';

import CardsFavourites from '../../components/Cards/CardsFavourites';



const FavoritePage = () => {
    const favourites = useSelector((state) => state.favorites.entities)   
    return (
        <div>
            {favourites.length>0 ? <CardsFavourites  /> : <h3>No Favorites Selected!</h3>}
        </div>
    )
}

export default FavoritePage;