import React, {useEffect } from 'react';

import Cards from '../../components/Cards/Cards';
import '../../App.scss';

import { useDispatch } from 'react-redux';

import { actionFetchProduct } from '../../reducers/main.reducer';


const MainPage = () => {
    const dispatch = useDispatch();
    
    
    useEffect(() => {
        dispatch(actionFetchProduct())
    })

    return (
        <div className="App">
            <Cards />
        </div>
    );
}

export default MainPage;