import * as yup from 'yup';

export const validationSchema = yup.object({
	name: yup
		.string('Enter your name')
		.required('Name is required')
		.min(2, "Name is too short"),
	surname: yup
		.string('Enter your surname')
		.required('Surname is required')
		.min(3, "Surname is too short"),
	age: yup
		.number('Enter your age')
		.required('Age is required')
		.min(18), 
	address: yup
		.string('Enter your address')		
		.required('Address is required')
		.min(3, "Address is too short"),
	phone: yup
		.string('Enter your phone number')
		.required('Phone is required'),
	phone2: yup
		.number('Enter your phone number')
		
		 


});
