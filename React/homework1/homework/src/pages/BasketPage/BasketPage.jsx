

import CardsBasket from '../../components/Cards/CardsBasket';
import { Formik, Form } from 'formik'
import {Input,PatternNumber} from '../../components/Form/Input';
import { validationSchema } from './validations.js'
import './BasketPage.scss'
import {useDispatch, useSelector} from 'react-redux'
import { purchased } from '../../reducers/customer.reducer';
import { basketRemoved } from '../../reducers/basket.reducer';


const BasketPage = () => {
    const basket = useSelector((state) => state.basket.entities)
    const products = useSelector((state)=>state.main.products)
    const customerData = useSelector((state) => state.customer.customer)    
    const dispatch = useDispatch();      
  

    return (

        <div className="page">

            <Formik
                initialValues={customerData}
                onSubmit={
                    (values) => {
                        console.log(`Customer Info:`, JSON.stringify(values))
                        dispatch(purchased(values))
                        
                        basket.forEach(element => {
                            dispatch(basketRemoved(element))
                            console.log('Purchased product:', JSON.stringify(products[element]))                            
                        });
                        
                       
                    }
                   
                }
                validationSchema={validationSchema}
            >
                {({ errors, touched }) => (
                    
                    <Form>
                        <fieldset className="form-block">
                            <legend>Customer's information</legend>

                            <Input className='mb-3' name='name' label='Name' placeholder='name' errors={errors} error={errors.name && touched.name} />
                            <Input className='mb-3' name='surname' label='Surname' placeholder='surname' errors={errors}  error={errors.surname && touched.surname} />
                            <Input className='mb-3' name='age' label='Age' placeholder='age' errors={errors}  error={errors.age && touched.age} />
                            <Input className='mb-3' name='address' label='Address' placeholder='address' errors={errors}  error={errors.address && touched.address} />
                            <Input className='mb-3' name='phone' label='Phone' placeholder='phone' errors={errors}  error={errors.address && touched.address} />
                      
                            <PatternNumber className='mb-3' name='phone2' label='Phone with Mask' placeholder='phone' errors={errors}  error={errors.phone2 && touched.phone2} />
                           
                        </fieldset>
                        <div className="col-12">
                            <button type="submit" className="btn btn-primary">Checkout</button>
                        </div>
                    </Form>
                )

                }

            </Formik>

            <div>
                {basket.length > 0 ? <CardsBasket /> : <h3>No Goods in Basket!</h3>}
            </div>
        </div>
    )
}

export default BasketPage;