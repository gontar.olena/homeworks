import { render, screen } from '@testing-library/react';
import Button from '../src/components/Button';
import Modal from '../src/components/Modal';
import { favoriteAdded, favoriteRemoved } from '../src/reducers/favourite.reducer';
import reducerFavourite from '../src/reducers/favourite.reducer';
import reducerBasket from '../src/reducers/basket.reducer';
import {basketAdded, basketRemoved} from '../src/reducers/basket.reducer';


describe(' snapshot', () => {
  test('Button', ()=> {
    const button = render(<Button />);
    expect(button).toMatchSnapshot();
  })



  test('Modal', ()=> {
    
    const modal = render(<Modal id={`test`} product={`{productTitle:1w}`} header="Your Card" closeButton={true} text={`text`} actions={<div><Button /><Button /></div>}/>);
    expect(modal).toMatchSnapshot();
  })
})

describe('button tests', () => {
  test('Button text', ()=>{
   
      const mockCallBack = jest.fn();    
      const {container} = render(<Button backgroundColor='black' textBtn="testText" onClick={() => {mockCallBack}}/>)
      
      expect(screen.getByText(/testText/i))
  })
  
  test('Button color', ()=>{
   
    const mockCallBack = jest.fn();    
    const {container} = render(<Button backgroundColor='black' textBtn="testText" onClick={() => {mockCallBack}}/>)
    expect(container.firstChild).toHaveStyle(`background: black`) 
   
})
})

describe('modal tests', () => {
  test('Modal text', ()=>{
   
      render(<Modal id={`test`} product={`{productTitle:1w}`} header="HeaderTest" closeButton={true} text="MainText" actions={<div><Button /><Button /></div>}/>);           
      expect(screen.getByText(/HeaderTest/i))
      expect(screen.getByText(/MainText/i))
  })
  
  test('Modal to have class modal', ()=>{   
      
    const {container} = render(<Modal id={`test`} product={`{productTitle:1w}`} header="HeaderTest" closeButton={true} text="MainText" actions={<div><Button /><Button /></div>}/>)
    expect(container.firstChild).toHaveClass("modal")
    
   
})
})

describe('reducer test', () => {
  test('add to favourite', ()=>{
    const prevState = {entities: []}
    expect(reducerFavourite(prevState,favoriteAdded(1))).toEqual({entities: [1]});
  });
  test('remove from favourite', ()=>{
    const prevState = {entities: [1,2]}
    expect(reducerFavourite(prevState,favoriteRemoved(2))).toEqual({entities: [1]});
  });
  test('add to basket', ()=>{
    const prevState = {entities: []}
    expect(reducerBasket(prevState,basketAdded(1))).toEqual({entities: [1]});
  });
  test('remove from basket', ()=>{
    const prevState = {entities: [1,2]}
    expect(reducerBasket(prevState,basketRemoved(2))).toEqual({entities: [1]});
  });
})
