import React from 'react';
import { Routes, Route } from 'react-router-dom';



import Header from './components/Header';
import MainPage from './pages/MainPage/MainPage';
import FavoritePage from './pages/FavoritePage/FavoritePage';
import BasketPage from './pages/BasketPage/BasketPage'

import './App.scss';
import LayoutProvider from './context';


const App = () => {

  return (
    <LayoutProvider>
      <div className="App">
        <Header />
        <Routes>
          <Route path='/' element={<MainPage />} />
          <Route path='/favorites' element={<FavoritePage />} />
          <Route path='/basket' element={<BasketPage />} />
          <Route path='*' element={<p>Упс, щось пішло не так</p>} />
        </Routes>
      </div>
    </LayoutProvider>
  );
}

export default App;
