
import Card from './Card';
import {useEffect } from 'react';
import './Cards.scss';
import { useDispatch, useSelector } from 'react-redux';
import { actionFetchProduct } from '../../reducers/main.reducer';
import { useContext } from 'react';
import { LayoutContext } from '../../context/index'

const CardsBasket = () => {
    const dispatch = useDispatch();
    const {layout} = useContext(LayoutContext) 
    
    useEffect(() => {
        dispatch(actionFetchProduct())
    }, [])

    const productData = useSelector((state) => state.main.products)
    const basket = useSelector((state) => state.basket.entities)
    const favourites = useSelector((state) => state.favorites.entities)
      
    const filterBasketCards = productData.filter((card) => basket.indexOf(card.productID) > -1);   
   
    const productCards = filterBasketCards.map((card) => <Card key={card.productID} cardProps={card} favorite = {favourites.indexOf(card.productID)>-1?true:false}/>)
    
  
    return(
        <>
        {layout && 
        <div className="products">           
        {productCards}            
        </div>
        }
           {!layout && 
        <div className="products_grid">           
        {productCards}            
        </div>
        }
        </>
    )
}

export default CardsBasket;