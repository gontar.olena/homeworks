
import Card from './Card';
import './Cards.scss';
import {useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { actionFetchProduct } from '../../reducers/main.reducer';
import { useContext } from 'react';
import { LayoutContext } from '../../context/index'


const CardsFavourites = () => {
    
    const dispatch = useDispatch();
    const {layout} = useContext(LayoutContext) 
    
    useEffect(() => {
        dispatch(actionFetchProduct())
    })
    const favourites = useSelector((state) => state.favorites.entities)
    const productData = useSelector((state) => state.main.products)
   
    const filterFavoriteCards = productData.filter((card) => favourites.indexOf(card.productID) > -1);
    
   
    const productCards = filterFavoriteCards.map((card) => <Card key={card.productID} cardProps={card} favorite = {true}/>)
    
    return(
        <>
        {layout && 
        <div className="products">           
        {productCards}            
        </div>
        }
           {!layout && 
        <div className="products_grid">           
        {productCards}            
        </div>
        }
        </>
    )
}

export default CardsFavourites;