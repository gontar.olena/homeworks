import React from 'react';

import Card from './Card';
import './Cards.scss';

import { useSelector } from 'react-redux';
import { useContext } from 'react';
import { LayoutContext } from '../../context/index'


const Cards = () => {
    const {layout} = useContext(LayoutContext)    
    const productData = useSelector((state) => state.main.products)
    const favorites = useSelector((state)=>state.favorites.entities)
       
    const productCards = productData.map((card) => <Card key={card.productID} cardProps={card} favorite = {favorites!==null?favorites.indexOf(card.productID)!==-1:false}/>)
    
    return(
        <>
        {layout && 
        <div className="products">           
        {productCards}            
        </div>
        }
           {!layout && 
        <div className="products_grid">           
        {productCards}            
        </div>
        }
        </>
    )
}

export default Cards;