import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Modal from '../Modal';
import Button from '../Button';
import { useSelector,  useDispatch } from 'react-redux';
import { favoriteAdded, favoriteRemoved } from '../../reducers/favourite.reducer';
import { basketAdded, basketRemoved} from '../../reducers/basket.reducer';
import { modalOpen, modalClose } from '../../reducers/modal.reducer';
import { useContext } from 'react';
import { LayoutContext } from '../../context/index'

const Card = ({ cardProps, favorite }) => { //треба забрати пропси
 
  const {layout} = useContext(LayoutContext)
  
  const { productTitle, price, URL, productNumber, color, productID } = cardProps;
  const [isFavorite, setIsFavorite] = useState(favorite)
  const isSelected = isFavorite ? "fa fa-star fa-2x checked" : "fa fa-star fa-2x"

  const dispatch = useDispatch();
  const basket = useSelector((state) => state.basket.entities)
//обробка улюблених
  console.log(`card`, layout)
  useEffect(() => {

    if (isFavorite) {
          dispatch(favoriteAdded(productID))            
      }
    
    else {
          dispatch(favoriteRemoved(productID))
      }
    
    window.dispatchEvent(new Event('storage'))
 
    //localStorage.setItem("favorites", JSON.stringify(favourites))
  }, [isFavorite])

//відкриття модального вікна по ІД
  const onClickModal = (id) => {
       dispatch(modalOpen(`${id}`));
   }
  
//закриття модального вікна по ІД
  const onClickClose = (id) => {
    dispatch(modalClose(`${id}`));
  }
//додавання в кошик  
  const addToCard = (id) => {
   // console.log(`productID`, id)  
    if (basket.indexOf(id) === -1){  
      dispatch(basketAdded(id))
    }
    else {
      alert("Such Product is already in the basket!")
    }
    onClickClose(`AddToCard${productID}`)
  }

//видалення з кошика  
  const removeFromCard = (id) =>{
    dispatch(basketRemoved(id))
    onClickClose(`AddToCard${productID}`)
    }
    
    
  const currentWindow = window.location.pathname
    //console.log(currentWindow)
  return (

    <div className="product__card">
     
      <Modal id={`remove${productID}`} product={cardProps} header="Remove From Basket" closeButton={false} text={`Do you want to remove this product from the card?`} actions={<div><Button backgroundColor='rgb(20,20,20)' textBtn='OK' onClick={() => removeFromCard(productID)} /><Button backgroundColor='rgb(20,20,20)' textBtn='Cancel' onClick={() => { onClickClose(`remove${productID}`) }} /></div>} />
      {layout &&  
      <>
      <p className="card__title">{productTitle}  </p>
      <p className="card__price">{price}</p>
      <div className="card__icon">
        <img src={URL} alt={URL} />
      </div>
      <p className="card__number">{productNumber}</p>
      <p className="card__color">{color}</p>
      <span onClick={() => setIsFavorite(!isFavorite)} className={isSelected}></span>
      {currentWindow !== '/basket' && <Button backgroundColor='rgb(20,20,20)' textBtn="Add to card" onClick={() => { onClickModal(`AddToCard${productID}`) }} />}
      {currentWindow === '/basket' && <Button backgroundColor="darkblue" textBtn= "Remove From Basket" onClick = {() => onClickModal(`remove${productID}`)}/>}

      </>
      }
       
       {!layout &&  
      <>
      <p className="card__number">{productNumber}</p>
      <p className="card__title">{productTitle}  </p>
      <p className="card__price">{price}</p>
      <p className="card__color">{color}</p>
      <span onClick={() => setIsFavorite(!isFavorite)} className={isSelected}></span>
      {currentWindow !== '/basket' && <Button backgroundColor='rgb(20,20,20)' textBtn="Add to card" onClick={() => { onClickModal(`AddToCard${productID}`) }} />}
      {currentWindow === '/basket' && <Button backgroundColor="darkblue" textBtn= "Remove From Basket" onClick = {() => onClickModal(`remove${productID}`)}/>}

      </>
      }
          
      <Modal id={`AddToCard${productID}`} product={cardProps} header="Your Card" closeButton={true} text={`Do you want to add this product to the card?`} actions={<div><Button backgroundColor='rgb(20,20,20)' textBtn='OK' onClick={() => addToCard(productID)} /><Button backgroundColor='rgb(20,20,20)' textBtn='Cancel' onClick={() => { onClickClose(`AddToCard${productID}`) }} /></div>} />
    </div>
  )
}

Card.propTypes = {
  cardProps: PropTypes.object.isRequired

}
export default Card;