import { ErrorMessage } from 'formik';
import cx from 'classnames';
import PropTypes from 'prop-types';
import { PatternFormat } from 'react-number-format';

import './Input.scss'
const PatternNumber = ({ type, placeholder, label, name, className, error, errors, ...restProps }) => {
    
    console.log(errors)
    console.log(error)
    
    return (
        <label className={cx("form-item", className, {'error-message': error})}>
            <p className='form-label'>{label}</p>
            <PatternFormat className = 'form-control' name = {name} format="(###)###-##-##" allowEmptyFormatting = {true} mask='#' />
            <ErrorMessage className='error-message' name ={name} component='p'/>
        </label>
    )
}


PatternNumber.defaultProps = {
    type:"text"    
}

PatternNumber.propTypes = {
    type:PropTypes.string, 
    placeholder:PropTypes.string, 
    label:PropTypes.string, 
    name:PropTypes.string, 
    className:PropTypes.string, 
    error:PropTypes.object,
   

}

export default PatternNumber