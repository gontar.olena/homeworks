import { Field, ErrorMessage } from 'formik';
import cx from 'classnames';
import PropTypes from 'prop-types';
import { PatternFormat } from 'react-number-format';

import './Input.scss'
const Input = ({ type, placeholder, label, name, className, error, errors, ...restProps }) => {
    
    return (
        <label className={cx("form-item", className, {'error-message': error})}>
            <p className='form-label'>{label}</p>
            <Field type ={type} className = 'form-control' name = {name} placeholder = {placeholder} {...restProps} />
            <ErrorMessage className='error-message' name ={name} component='p'/>
        </label>
    )
}


Input.defaultProps = {
    type:"text"    
}

Input.propTypes = {
    type:PropTypes.string, 
    placeholder:PropTypes.string, 
    label:PropTypes.string, 
    name:PropTypes.string, 
    className:PropTypes.string, 
    error:PropTypes.object,
   

}

export default Input