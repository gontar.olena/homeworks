import React from 'react';
import Button from '../Button';

import './Modal.scss';



const Modal = ({id, header, closeButton, text, actions, product})  => {
    const  onClickClose = ()=>{
                let modal = document.querySelector(`#${id}`);   
                modal.style.display = "none";
            }
    const {productTitle}=product;
    
    
    return (
        <div id={id} className="modal">  
            <div className="modal-content">
                <div className="modal-header">                             
                    <h4>{header}</h4>
                    {closeButton && 
                    <div className = "close">
                        <Button backgroundColor="#FFFFFF00" textBtn= "&times;" onClick = {onClickClose}/>
                    </div>
                    }                        
                </div>
                <div className="modal-body">
                    <p>{text}</p>
                    {productTitle && <h3>{productTitle}</h3>}                    
                </div>
                <div className="modal-footer">
                    {actions}
                </div>                
            </div>   
        </div>                   
    )  
}
export default Modal;