import React, {Component} from 'react';

import './Button.scss';

class Button extends Component{       
    
    render(){
        const {backgroundColor, textBtn, onClick} = this.props;
        const btnStyle = {backgroundColor};
        
        return (
        <button className="button" type="button" style={btnStyle} onClick={onClick}>{textBtn}</button>
        )      
    }
}

export default Button