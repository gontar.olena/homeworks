
import "./Header.scss"
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useContext, useState } from 'react';
import { LayoutContext } from '../../context/index'
import api from "../../api";

const Header = () => {


  const basket = useSelector((state) => state.basket.entities)
  const favourites = useSelector((state) => state.favorites.entities)
  const { layout, newLayout } = useContext(LayoutContext)
  const [checked, setChecked] = useState(layout)


  const handleChange = () => {
    console.log(`after setChecked`, checked)
    setChecked(!checked)
    console.log(`after setChecked`, checked)
    handleLayout()
    

  }
  const handleLayout = async () => {
    const updateLayout = await api.changeLayout(checked)
    console.log(`updateLayout`, updateLayout)
    newLayout(updateLayout)
  }
  return (
    <div className="header">
      <div>
        <Link to={'/'}><p className="title">Select your new phone</p></Link>
        <span className = "switch-label">Change Layout</span>
        <label className="switch">
          <input type="checkbox" value={checked} onChange={handleChange} />
          <span className="slider"></span>
        </label>
      </div>
      <div>
        <Link to={`/favorites`}><p className="fa fa-heart fa-solid">{favourites ? favourites.length : 0}</p></Link>
        <Link to={`/basket`}><p className="fa fa-shopping-basket fa-solid"> {basket ? basket.length : 0} </p></Link>
      </div>
    </div>
  )
}

export default Header;