import { configureStore } from '@reduxjs/toolkit';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

import favouriteReducer from '../reducers/favourite.reducer';
import mainReducer from '../reducers/main.reducer';
import basketReducer from '../reducers/basket.reducer';
import modalReducer from '../reducers/modal.reducer';
import customerReducer from '../reducers/customer.reducer';

//MIDDLEWARE
const localStorageMiddleware = ({ getState }) => {
    return next => action => {
        const result = next(action);
        localStorage.setItem('favorites', JSON.stringify(getState().favorites.entities));
        localStorage.setItem('basket', JSON.stringify(getState().basket.entities));
        return result;
    };
};

let preloadedFavorites
let preloadedBasket


const preloadedFavoritesString = localStorage.getItem("favorites")
const preloadedBasketString = localStorage.getItem("basket")



if (preloadedFavoritesString) {
    preloadedFavorites = JSON.parse(localStorage.getItem("favorites"))
} else {
    preloadedFavorites = []
}

if (preloadedBasketString) {
    preloadedBasket = JSON.parse(localStorage.getItem("basket"))
} else {
    preloadedBasket = []
}

let preloadedState = {
    favorites: { entities: preloadedFavorites },
    basket: { entities: preloadedBasket},
   
}
console.log(preloadedState)

const store = configureStore({
    reducer: {
        favorites: favouriteReducer,//favorite.reducer.js
        basket: basketReducer,
        main: mainReducer,
        modal: modalReducer,
        customer: customerReducer
    },
    preloadedState,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger, thunk, localStorageMiddleware)


})

export default store;