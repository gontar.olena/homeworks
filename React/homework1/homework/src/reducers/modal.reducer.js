import {createSlice} from "@reduxjs/toolkit";


const initialState = {    
    status: 'closed'
}

const modalSlice = createSlice({
    name: "modal",
    initialState,
    reducers:{
        modalOpen(state, action){                         
            let modal = document.querySelector(`#${action.payload}`);  
            modal.style.display = 'block' 
            state.status = 'opened'
                              
        },
        modalClose(state, action){            
            let modal = document.querySelector(`#${action.payload}`);  
            modal.style.display = 'none' 
            state.status = 'closed' 
        }
    }
    
})

export const {modalOpen, modalClose} = modalSlice.actions;

export default modalSlice.reducer




