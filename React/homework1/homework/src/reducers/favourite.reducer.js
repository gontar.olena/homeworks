import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    entities: []
}

const favouriteSlice = createSlice({
    name: "favorites",
    initialState,
    reducers:{
        favoriteAdded(state, action){
            if (state.entities.indexOf(action.payload) === -1) {                
                state.entities.push(action.payload)            
              
        }},
        favoriteRemoved(state, action){
            if (state.entities.indexOf(action.payload) !== -1) {                
                state.entities.splice(state.entities.indexOf(action.payload),1)                
                
            }
        }
    }
    
})

export const {favoriteAdded, favoriteRemoved} = favouriteSlice.actions;

export default favouriteSlice.reducer




