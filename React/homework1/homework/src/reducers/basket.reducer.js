import {createSlice} from "@reduxjs/toolkit";


const initialState = {
    entities: []
}

const basketSlice = createSlice({
    name: "basket",
    initialState,
    reducers:{
        basketAdded(state, action){                         
                state.entities.push(action.payload)                   
        },
        basketRemoved(state, action){            
                state.entities.splice(state.entities.indexOf(action.payload),1)   
        }
    }
    
})

export const {basketAdded, basketRemoved} = basketSlice.actions;

export default basketSlice.reducer




