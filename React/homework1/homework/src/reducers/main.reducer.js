import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";

const urlProduct = window.location.origin + '/Products.json';

export const actionFetchProduct = createAsyncThunk('product/fetch_data', async () => {
    let response = await fetch(urlProduct);
    let {products} = await response.json();
    return products
})

const initialState = {
    products: [],
    loader: true,
}

const mainSlice = createSlice({
    name: "main",
    initialState,
    reducers: {
        actionMain: (state,{payload}) => {
            
        }, // actions, reducer
    }, 
    extraReducers: (builder)=>{
        builder.addCase(actionFetchProduct.pending, (state) => {
            state.loader = true;
        })

        builder.addCase(actionFetchProduct.fulfilled, (state, {payload}) => {
            state.products = payload;
            state.loader = false;
        })
    }

})

export const {actionMain} = mainSlice.actions

export default mainSlice.reducer 