import { createContext, useState } from "react";

export const LayoutContext = createContext({})
    
const LayoutProvider= ({children}) =>{
    const [layout, setLayout] = useState(false) 
    
    const newLayout = (value) =>{
       setLayout(value)
    }

    return (
    <LayoutContext.Provider value={{layout, newLayout}}>
    {children}
    </LayoutContext.Provider>
    )
}

export default LayoutProvider