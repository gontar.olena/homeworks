// ## Теоретичні питання

// 1. Чому для роботи з input не рекомендується використовувати клавіатуру?
// Існують інші способи введення крім клавіатури, вставлення і копіювання, розпізнавання мови і т.д. Тому використання лише клавіатури може бути недостатнім

// ## Завдання

// Реалізувати функцію підсвічування клавіш. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:
// - У файлі `index.html` лежить розмітка для кнопок.
// - Кожна кнопка містить назву клавіші на клавіатурі
// - Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням `Enter` перша кнопка забарвлюється у синій колір. Далі, користувач натискає `S`, і кнопка `S` забарвлюється в синій колір, а кнопка `Enter` знову стає чорною. 

// #### Література:
// -  [Клавіатура: keyup, keydown, keypress](https://learn.javascript.ru/keyboard-events )

let btnCollection = document.querySelectorAll(".btn")
let btnKeys = {};
btnCollection.forEach(element => {
    if (element.innerText.length > 1) {
        btnKeys[element.innerText] = element
    } 
    else {
        btnKeys[`Key`+ element.innerText] = element
    }
});

document.addEventListener("keyup", (event)=> {
    
    for (const key in btnKeys) {
        btnKeys[key].classList.remove("pressed")                
    }

    for (const key in btnKeys) {
        if (event.code in btnKeys) {
            btnKeys[event.code].classList.add("pressed")    
       }      
    }        
})