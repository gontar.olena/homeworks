// ## Теоретичні питання

// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
    // Екранування використовується для виведення спеціальних символів без обробки, як є, буквально.

    // 2. Які засоби оголошення функцій ви знаєте?
    //  function declaration 
             // function sayHello(){}
    // functrion expression 
             // const sayHello = function(){}
    // function-arrows
             // const sayHello =() => {}

    // 3. Що таке hoisting, як він працює для змінних та функцій?
             // Це оголошення всіх змінних на початку коду. 

    // ## Завдання

// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Візьміть виконане домашнє завдання номер 4 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
//   1. При виклику функція повинна запитати дату народження (текст у форматі `dd.mm.yyyy`) і зберегти її в полі `birthday`.
//   2. Створити метод `getAge()` який повертатиме скільки користувачеві років.
//   3. Створити метод `getPassword()`, який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`.
// - Вивести в консоль результат роботи функції `createNewUser()`, а також функцій `getAge()` та `getPassword()` створеного об'єкта.

// #### Література:

// - [Дата і час](https://learn.javascript.ru/datetime)

const createNewUser = () => {
    firstName = prompt("Enter your first name: ")
    lastName = prompt("Enter your last name: ")
    birthday = prompt("Enter you birthday in format dd.mm.yyyy")
   
    const newUser = {
            firstName:firstName,
            lastName:lastName,
            birthday: new Date(birthday.slice(6),birthday.slice(3,5),birthday.slice(0,2)),
            
            getAge(){
                let today = new Date()
                //find when the birthday is this year. 
                let currentBirthday = new Date(today.getFullYear(),this.birthday.getMonth(),this.birthday.getDate())
                //calculate the age depending on the birthday this year.
                if (today<currentBirthday){
                    return today.getFullYear() - this.birthday.getFullYear()- 1
                }
                else{
                    return today.getFullYear() - this.birthday.getFullYear()
                }                
            },
            getLogin(){
            return (this.firstName[0] + this.lastName).toLowerCase()
            },  
            getPassword(){
            return this.firstName[0].toUpperCase()+this.lastName.toLowerCase()+this.birthday.getFullYear()    
            },
            setFirstName(value){
                Object.defineProperty(this,"firstName",{writable:true})
                this.firstName = value
                Object.defineProperty(this,"firstName",{writable:false})
             },

            setLastName(value){
                Object.defineProperty(this,"lastName",{writable:true})
                this.lastName = value
                Object.defineProperty(this,"lastName",{writable:false})
            }
            
    }
    Object.defineProperty(newUser,"firstName",{writable:false})
    Object.defineProperty(newUser,"lastName",{writable:false})
    return newUser
}

newUser = createNewUser()
console.log(newUser.getAge())
console.log(newUser.getLogin())
console.log(newUser.getPassword())


