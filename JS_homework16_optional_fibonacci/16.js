// ### Дане завдання не обов'язкове для виконання

// ## Завдання

// Реалізувати функцію для підрахунку n-го узагальненого числа Фібоначчі. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:
// - Написати функцію для підрахунку n-го узагальненого [числа Фібоначчі.](https://ru.wikipedia.org/wiki/%D0%A7%D0%B8%D1%81%D0%BB%D0%B0_%D0%A4%D0%B8%D0%B1%D0%BE%D0%BD%D0%B0%D1%87%D1%87%D0%B8). Аргументами на вхід будуть три числа - `F0`, `F1`, `n`, де `F0`, `F1` - перші два числа послідовності (можуть бути будь-якими цілими числами), `n` - порядковий номер числа Фібоначчі, яке треба знайти. Послідовність будуватиметься за наступним правилом `F2 = F0 + F1`, `F3 = F1 + F2` і так далі.
// - Отримати за допомогою модального вікна браузера число, яке введе користувач (`n`).
// - За допомогою функції порахувати n-е число в узагальненій послідовності Фібоначчі та вивести його на екран.
// - Користувач може ввести від'ємне число - результат треба порахувати за таким самим правилом (`F-1 = F-3 + F-2`).

// #### Література:
// - [Рекурсія, стек](https://learn.javascript.ru/recursion)
let myNumber = prompt("Input integer number")

while (!Number.isInteger(Number(myNumber)) || myNumber == ''){
    myNumber =prompt(`Your input was not a  integer. Input  integer number:`, myNumber)
}

function Fibonachi(f0, f1, number) {
    if(number == null){
        return "Fibonachi number is not defined"
    }
    const n = Number(number)
    if (n == 0){
        return f0
    } 
    
    if (n == 1) {
        return f1
    }
    
    if(n > 1){
        return Fibonachi(f0, f1, n - 1) + Fibonachi(f0, f1, n-2)
    }
    if(n < 0){
        return Fibonachi(f0, f1, n + 2) - Fibonachi(f0, f1, n + 1)
    }
    
}

console.log(Fibonachi(0, 1, myNumber))
