// ## Теоретичні питання

// 1. Описати своїми словами навіщо потрібні функції у програмуванні.
//  Якщо одну і ту саму логіку, дію потрібно виконати в різних місцях програми, для того, щоб не писати один і той же код кілька разів, зручно використовувати функцію.
// 2. Описати своїми словами, навіщо у функцію передавати аргумент.
//  Аргументи дозволяють використовувати функцію для різних вхідних даних. Їх значення передається локальним змінним, які використовуються у функції для розрахунку її результату. 
// 3. Що таке оператор return та як він працює всередині функції?
// Оператор return  повертає значення функції у те місце коду, звідки викликалась функція. В коді функції він може зустрічатися кілька разів і перше його виконання в коді функції одразу закінчує виконаня коду функції.

// ## Завдання

// Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React. 

// #### Технічні вимоги:

// - Отримати за допомогою модального вікна браузера два числа.
// - Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено `+`, `-`, `*`, `/`.
// - Створити функцію, в яку передати два значення та операцію.
// - Вивести у консоль результат виконання функції.

// #### Необов'язкове завдання підвищеної складності

// - Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів числа, або при вводі вказав не числа, - запитати обидва числа знову (при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше).

// #### Література:

// - [Функції - основи](https://learn.javascript.ru/function-basics)
let number1 = prompt("Enter your number1:")
let number2 = prompt("Enter your number2:")

while (((isNaN(number1)) || (number1=='')) || 
        ((isNaN(number2)) || (number2==''))){
            alert("Entered numbers are invalid. Enter integer numbers:")
            number1 = prompt("Enter your number1:",number1)
            number2 = prompt("Enter your number2:",number2)
            
 }


const functionOperation = (x, y, operation) => {
switch (operation){
case '+': 
    return `Sum of ${x} and ${y} is ${x+y};`
    //break; не використовую, бо return 

case '-':
    return `Difference of ${x} and ${y} is ${x-y}`;
    

case '*':
    return `Muliplication of ${x} and ${y} results in ${x*y}`;
    

case '/':
    return `Devision of ${x} by ${y} results in ${x/y}`;
   

default:
    return "Operation is not defined";
}
}

if (number1===null || number2===null){
    alert("Operation canceled")
}
else{
    let userOperation = prompt("Input operation: `+`, `-`, `*`, `/`")
   
    console.log(functionOperation(Number(number1),Number(number2),userOperation))
}

