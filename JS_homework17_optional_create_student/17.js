// ### Дане завдання не обов'язкове для виконання

// ## Завдання

// Створити об'єкт "студент" та проаналізувати його табель. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:
// - Створити порожній об'єкт `student`, з полями `name` та `lastName`.
// - Запитати у користувача ім'я та прізвище студента, отримані значення записати у відповідні поля об'єкта.
// - У циклі запитувати у користувача назву предмета та оцінку щодо нього. Якщо користувач натисне Cancel при n-питанні про назву предмета, закінчити цикл. Записати оцінки з усіх предметів як студента `tabel`. 
// - порахувати кількість поганих (менше 4) оцінок з предметів. Якщо таких немає, вивести повідомлення "Студент переведено на наступний курс".
// - Порахувати середній бал з предметів. Якщо він більше 7 – вивести повідомлення `Студенту призначено стипендію`.

const student = {
    name:'',
    lastName: '',
}

let studentName = prompt("Enter students name:")
let studentLastName = prompt("Enter student last name:")

while (studentName == '' || studentLastName == ''){
    studentName = prompt("Enter correct student's name:", studentName)
    studentLastName = prompt("Enter correct student's last name:", studentLastName)
}
student.name = studentName
student.lastName = studentLastName

if (studentName === null || studentLastName === null){
    alert("No data about student")
}
else {
    student.tabel={}
    let subject = prompt("Enter subject name:")
    while (subject !== null){
        mark = prompt(`Enter mark for subject ${subject}`)
        while (isNaN(mark) || mark === '' || mark === null){
            mark = prompt(`Your entry is not correct, enter mark for subject ${subject}`, mark)
        }
        student.tabel[subject] = mark
        subject = prompt("Enter next subject name:")
    }
    
}
let badmarks = 0
let summarks = 0

for (let subject in student.tabel){
    if(student.tabel[subject]< 4){
        badmarks += 1
    }
    summarks += Number(student.tabel[subject])
}
let averagemarks = summarks/Object.keys(student.tabel).length

if (badmarks===0) {
    console.log(`Студента ${student.lastName} ${student.name} переведено на наступний курс`)
}
if(averagemarks>7)
console.log(`Студенту ${student.lastName} ${student.name} призначено стипендію`)