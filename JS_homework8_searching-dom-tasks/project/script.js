// ## Теоретичні питання
// 1. Опишіть своїми словами що таке Document Object Model (DOM)
    // Це повна структура html документа, що зберігає ієрархію та дозволяє шукати необхідні елементи в документі
// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
    // innerHTML - повертає код, innerText - власне текст, при записі цих властивостей, innerText запише текст як є, innerHTML - конвертує в код при потребі
// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
    // через методи getElementByID(), getElementByClass(), getElementByTagName(), querySelector(), querySelectorAll(). querySelector більш унівенрсальні, оскільки роблять пошук по CSS Selector, дозволяючи таким чином об'єдувати кілька селекторів. 

// ## Завдання

// Код для завдань лежить в папці project.

// 1) Знайти всі параграфи на сторінці та встановити колір фону #ff0000 - done

// 2) Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

// 3) Встановіть в якості контента елемента з класом testParagraph наступний параграф - <p>This is a paragraph<p/>

// 4) Отримати елементи <li>, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

// 5) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.  

// #### Література:

// - [Пошук DOM елементів](https://uk.javascript.info/searching-elements-dom)

//1
const allparagraph = document.getElementsByTagName('p')

for (const element of allparagraph) {
    element.style.backgroundColor = "#ff0000";
}
//2
const optionListitem = document.getElementById('optionsList')
console.log(optionListitem)
console.log(optionListitem.parentNode)
const optionListChildNodes = optionListitem.childNodes
if (optionListChildNodes.length !== 0) {
    console.log(optionListChildNodes)
}
//3 testParagraph не клас, а id
const newParagraph = document.querySelector('#testParagraph')
newParagraph.innerHTML = '<p>This is a paragraph<p/>'

//4

const liItems = document.querySelectorAll('.main-header li')
console.log(liItems)
liItems.forEach(element => {element.classList.add('nav-item')
    });
console.log(liItems)

//5 .section-title - no such class in the project
const sectionItems = document.querySelectorAll('.section-title')
if (sectionItems.length === 0) {
    console.log('No such elements in the DOM')
} else
{sectionItems.forEach(element => {element.classList.remove('section-title')})}
console.log(sectionItems)

