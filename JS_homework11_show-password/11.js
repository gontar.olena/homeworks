
// ## Завдання

// Написати реалізацію кнопки "Показати пароль". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:
// - У файлі `index.html` лежить розмітка двох полів вводу пароля.
// - Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
// - Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
// - Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
// - Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// - Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – `You are welcome`;
// - Якщо значення не збігаються - вивести під другим полем текст червоного кольору `Потрібно ввести однакові значення`
// - Після натискання на кнопку сторінка не повинна перезавантажуватись
// - Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.

let formPassword = document.querySelector(".password-form")
formPassword.addEventListener("click",(event)=>{
    let newClass = ""
    let newType =""
    if (event.target.nodeName === "I"){
        if(event.target.classList.contains("fa-eye")){
            event.target.classList.remove("fa-eye")
            newClass = "fa-eye-slash"
            newType = "text"
            
        }
        if(event.target.classList.contains("fa-eye-slash")){
            event.target.classList.remove("fa-eye-slash")
            newClass = "fa-eye"
            newType="password"
           
        }
        event.target.classList.add(newClass)
        event.target.previousSibling.previousSibling.setAttribute("type", newType)
             
    }

})

let confirmButton = document.querySelector(".btn")
confirmButton.addEventListener("click", (event) => {
    let password1 = document.querySelector('input[id="password1"]').value
    let password2 = document.querySelector('input[id="password2"]').value
    event.preventDefault();
    event.stopPropagation();
   
    if (password1===password2){
        alert(`You are welcome`)
    } else {
        confirmButton.insertAdjacentHTML("beforebegin", `<span class = "error-message" style = "color:red">Потрібно ввести однакові значення</span>`)
        // додати реакцію на клік
    }
document.addEventListener('click', ()=> {
    let errorMessage = document.querySelector(".error-message")
    if (!(errorMessage === null)){
        errorMessage.remove()
        document.querySelector('input[id="password1"]').value = ""
        document.querySelector('input[id="password2"]').value = ""
    }
})

})
