// ## Теоретичні питання

// 1. Опишіть, як можна створити новий HTML тег на сторінці.
    //  Створити новий елемент на сторінці можна за допомогою методу .createElement(tag), і потім опублікувати його на сайті за допомогою одного з методів append, prepend, before, after.
    //  Інший спосіб: elem.insertAdjacentHTML(where, html), цей метод дозволяє додавати html код на сторінку в те місце куди вкаже параметр where. 
// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
    // Перший параметр функції відповідає за місце, куди буде вставлено код з другого параметра по відношенню до елемента, beforebegin, afterbegin, beforeend, afterend
// 3. Як можна видалити елемент зі сторінки?
    // Видалити елемент зі сторінки можна за допомогою методу elem.remove()

// ## Завдання

// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// - кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

// Приклади масивів, які можна виводити на екран:

// ```javascript
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ```

// ```javascript
// ["1", "2", "3", "sea", "user", 23];
// ```

// - Можна взяти будь-який інший масив.

// #### Необов'язкове завдання підвищеної складності:

// 1. Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список.
//    Приклад такого масиву:

//    ```javascript
//    ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
//    ```

//    > Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.

// 2. Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.

// #### Література:

// - [Пошук DOM елементів](https://uk.javascript.info/searching-elements-dom)
// - [Додавання та видалення вузлів](https://uk.javascript.info/modifying-document)
// - [Шаблонні рядки](http://learn.javascript.ru/es-string)
// - [Array.prototype.map()](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/map)
// - [setTimeout, setInterval](https://learn.javascript.ru/settimeout-setinterval)



const simpleArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]
const myArray = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", ["Zolochiv", "Truskavets"],"Dnieper"];
const arrayToHtml = (array) => {
    let textHTML = ""
    array.forEach(item => {
    if (typeof item ==='object'){
        textHTML += `${arrayToHtml(item)}`
    }
    else
    {
        textHTML +=`<li>${item}</li>`
    }
    });
    return `<ul>${textHTML}</ul>`
} 

const createList=(array, elem=document.querySelector('body'))=>{
       elem.insertAdjacentHTML("beforeend", arrayToHtml(array)) 
        
}

createList(simpleArray)
createList(myArray)


let delay = 5000;
document.body.insertAdjacentHTML("beforeend",`<p class="timer">TIMER Started</p>`)
const myTimer = document.querySelector('.timer')
setTimeout(function timer(){
    
    if (delay === 0){
        document.body.innerHTML = "" 
    }
    else{
        
       
        myTimer.innerText = `TIMER - ${delay/1000}`
        setTimeout(timer, 1000)
    }
    delay -= 1000
},1000)






