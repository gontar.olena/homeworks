// ## Дане завдання не обов'язкове для виконання

// ## Теоретичні питання

// 1. Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?
    //Рекусрсія - це вкладена функція сама в себе, тобто виклик функції є всередині самої себе.

// ## Завдання

// Реалізувати функцію підрахунку факторіалу числа. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Отримати за допомогою модального вікна браузера число, яке введе користувач.
// - За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
// - Використовувати синтаксис ES6 для роботи зі змінними та функціями.

// #### Необов'язкове завдання підвищеної складності
// - Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів число, або при введенні вказав не число, - запитати число заново (при цьому значенням для нього повинна бути введена раніше інформація).

// #### Література:
// - [Рекурсія, стек](https://learn.javascript.ru/recursion)

let myNumber = prompt("Input positive integer number")

while (!Number.isInteger(Number(myNumber)) || myNumber <= 0){
    myNumber =prompt(`Your input was not a positive integer. Input positive integer number:`, myNumber)
}

function factorial(n) {
    if (n<=0){
        throw new Error("factorial is not defined")
    }
    let result = n
    if (n == 1) {
        return result
    }
    else {
        return result * factorial(n - 1)
       
    }
}

console.log(factorial(myNumber))


