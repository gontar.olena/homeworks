const API = 'https://ajax.test-danit.com/api/swapi/films'

const filmList = document.querySelector(".film-list")
const sendRequest = async (url, method='GET', config) => {
    return await fetch(url,{method, ...config})
        .then(response => { 
           
            if(response.ok){
                return response.json()
            } else {
                return new Error ('Something is wrong')
            }
        })
}

const getFilms = () => sendRequest(API);
//get list of films
getFilms().then((data) =>{
    let films = "";
    data.forEach(({episodeId, name, openingCrawl}) => {
        films += `<li> <span class = "bold-name">Episode: ${episodeId}, Name: ${name}</span> <p>${openingCrawl}</p><ul  class = "episode-${episodeId}"></ul>
        <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        </li>`
        
    });
  
    filmList.insertAdjacentHTML("beforeend", films);
    data.forEach(({episodeId, characters}) => {
              
        //create array of promises for characters
        const getCharacter =[];
        characters.forEach((item)=>{
            const getSingleCharacter = () => sendRequest(item);
            getCharacter.push(getSingleCharacter());
           
        }) 
        
        //getting all characters
        Promise.all(getCharacter).then(values =>{
        values.forEach (({id, name, gender, mass, skinColor, eyeColor}) => {
            let characterItem = `<li> <span class="bold-name"> ${name}</span> <p>gender: ${gender}, mass: ${mass}, skinColor: ${skinColor}, eyeColor: ${eyeColor}</p> </li>`            
            document.querySelector(`.episode-${episodeId}`).insertAdjacentHTML('beforeend',characterItem)
        })
        document.querySelector(`.episode-${episodeId} + .lds-spinner`).style.display="none";
       })
       
        

    })
}
)



