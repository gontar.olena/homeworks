
// ## Теоретичне питання
// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію `try...catch`.

// Конструкцію `try...catch` доречно використовувати для опрацювання помилок, при цьому не зупиняючи виконання коду.
// Наприклад, перебір і опрацювання файлів, перебір масивів, опрацювання відомих помилок.

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
     price: 40
    }
  ];

const arrayToHtml = (array) => {
    let textHTML = "";
    array.forEach((item, index) => {
        try {
            if (!Object.keys(item).includes("author")){throw `Position ${index} with error: there is no author!`}
            if (!Object.keys(item).includes("name")){throw `Position ${index} with error: there is no name!`}
            if (!Object.keys(item).includes("price")){throw `Position ${index} with error: there is no price!`}
            textHTML = `${textHTML} <li> Автор: ${item.author}, назва: ${item.name}, ціна: ${item.price} </li>`
        } catch (error) {
            console.log(error)
        }
    });
   
    return `<ul>${textHTML}</ul>`
} 

const createList=(array, elem=document.querySelector('body'))=>{
       elem.insertAdjacentHTML("beforeend", arrayToHtml(array)) 
        
}

createList(books)



