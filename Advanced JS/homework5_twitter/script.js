const API = 'https://ajax.test-danit.com/api/json/';

const postsWrapper = document.querySelector('.post-wrapper'),
         btnAdd = document.querySelector('.add-post'),
         modal = document.querySelector('.modal'),
         modalClose = document.querySelector('.close')
         btnModalSubmit = document.querySelector('[type="submit"]')     
 

const sendRequest = async (entity, method = "GET", config) => {
    return await fetch(`${API}${entity}`, {  //{}
        method,
        ...config
    }).then(response => {
        if(response.ok){
            if(method !== 'DELETE'){
                return response.json()
            } else {
                return response
            }
        } else {
            return new Error('Something went wrong');
        }
    })
}

const getUser = (id) => sendRequest(`users/${id}`);
const getUsers = () => sendRequest(`users`);
const getPosts = () => sendRequest('posts');
const deletePost = (postId) => sendRequest(`posts/${postId}`, 'DELETE');
const newPost = (requestBody) => sendRequest('posts', 'POST', {body: JSON.stringify(requestBody)});
const editPost = (postId, requestBody) => sendRequest(`posts/${postId}`, 'PUT', {
    body: JSON.stringify(requestBody)
})


Promise.all([getPosts(),getUsers()]).then((data) => {
    const posts = data[0];
    const users = data[1];
    console.log(posts);
    console.log(users);
    let postsList = "";
    posts.forEach(({userId:postsUserId, id:postId, title, body}) => {
        let {id, name, email} = users.find(({id:usersUserId})=>usersUserId===postsUserId)
        console.log(id, name, email)
        //заповнюємо дані
       postsList += `<div class="card" id = "post-${postId}">
            <div class="card-body">
                    <div class="card-navigation">
                        <div class="edit">edit</div>
                        <div class="save">save</div>
                        <div class="del">del</div>
                    </div>
                    <div class="user-name">User ${id}: ${name}</div>
                    <div class="user-mail">EMail: ${email}</div>
                    <div class="title">${title}</div>
                    <div class="body">${body}</div>
                </div>
            </div> `
             
    });
    postsWrapper.insertAdjacentHTML("afterbegin",postsList)
    document.querySelector(".lds-spinner").style.display = 'none';
    btnAdd.style.display ='block'
});


postsWrapper.addEventListener('click',(event)=>{
    //delete post
    if (event.target.classList.contains("del")){
        console.log(event.target.parentElement.parentElement.parentElement.id)
        const postId = event.target.parentElement.parentElement.parentElement.id.split("-")[1]
        deletePost(postId).then(response => {
            console.log(response);
            // console.log( document.getElementById(`${postId}`))
            document.getElementById(`post-${postId}`).innerHTML = ""
            console.log(response.ok, `deleted post # ${postId}`)
         })
    }
    //edit post
    if (event.target.classList.contains("edit")){
        console.log(event.target.parentElement.parentElement.parentElement.id)
        const postId = event.target.parentElement.parentElement.parentElement.id.split("-")[1]
        const postTitle = document.querySelector(`#post-${postId} .title`)
        const postBody = document.querySelector(`#post-${postId} .body`)
        const btnEdit = document.querySelector(`#post-${postId} .edit`)
        const btnSave = document.querySelector(`#post-${postId} .save`)
        const title = postTitle.innerText
        const body = postBody.innerText
       
        // console.log(postId, title, body)
        postTitle.innerHTML = `<label for="title">Change your post ${postId} title </label><input id="title" type="text" name="title" value="${title}">`
        postBody.innerHTML = `<label for="body">Change your post ${postId} : </label><textarea type="text" name="title" id="body" rows="3" columns="50">${body}</textarea>`
    
        btnEdit.style.display = 'none';
        btnSave.style.display = 'block';
    }

    //save post
    if (event.target.classList.contains("save")){
        console.log(event.target.parentElement.parentElement.parentElement.id)
        const postId = event.target.parentElement.parentElement.parentElement.id.split("-")[1]
        const postTitle = document.querySelector(`#post-${postId} .title`)
        const postBody = document.querySelector(`#post-${postId} .body`)
        const btnEdit = document.querySelector(`#post-${postId} .edit`)
        const btnSave = document.querySelector(`#post-${postId} .save`)
        const title = postTitle.querySelector('input').value
        const body = postBody.querySelector('textarea').value
       
        const requestBody = {
            title,
            body
        };
    
        editPost(postId, requestBody)
            .then(response => {
                console.log(response);
            })

        // console.log(postId, title, body)
        postTitle.innerText = title
        postBody.innerText = body

        btnEdit.style.display = 'block';
        btnSave.style.display = 'none';
    }
})


//open modal window
btnAdd.addEventListener("click", (event)=>{
    modal.style.display ="block";    
})

//close modal window
modalClose.onclick = function() {
      modal.style.display = "none";
      document.querySelector('[id="ptitle"]').value = ""
      document.querySelector('[id="ptext"]').value = ""
    }

//close modal if click out of modal-content
window.onclick = function(event) {
   
    if (event.target == modal) {
     modal.style.display = "none";
     document.querySelector('[id="ptitle"]').value = ""
     document.querySelector('[id="ptext"]').value = ""
   }
}

//creation of new post
btnModalSubmit.onclick = function(event){
    event.preventDefault();
    const postTitle = document.querySelector('[id="ptitle"]').value
    const postText = document.querySelector('[id="ptext"]').value

    const requestBody = {
        userId: 3,
        id: 101,
        body: postText,
        title: postTitle
    };


    newPost(requestBody)
        .then(({userId, id, body, title}) => {
            getUser(userId).then(
                ({name, email}) => {const postsList = `<div class="card" id = "post-${id}">
                <div class="card-body">
                        <div class="card-navigation">
                            <div class="edit">edit</div>
                            <div class="save">save</div>
                            <div class="del">del</div>
                        </div>
                        <div class="user-name">User ${userId}: ${name}</div>
                        <div class="user-mail">EMail: ${email}</div>
                        <div class="title">${title}</div>
                        <div class="body">${body}</div>
                    </div>
                </div> `
                postsWrapper.insertAdjacentHTML("afterbegin",postsList)
                modal.style.display ="none"
                document.querySelector('[id="ptitle"]').value = ""
                document.querySelector('[id="ptext"]').value = ""}
            )            
        })
}






  
    
    

