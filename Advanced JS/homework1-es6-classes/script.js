// ## Теоретичне питання
// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Прототипне наслідуванн дозволяє дочірньому об'єкту базуватися на іншому, використовуючи його методи.
// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
//super() викликається для того, щоб скопіювати необхідні поля з батьківського класу.

// ## Завдання
// 1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// 2. Створіть гетери та сеттери для цих властивостей.
// 3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// 4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// 5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

// ## Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Література:
// - [Класи на MDN](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Classes)
// - [Класи в ECMAScript 6](https://frontender.info/es6-classes-final/)

class Employee{
    constructor (name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name(){
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary(){
        return this._salary;
    }

    set name(value){
        this._name = value; 
    }

    set age(value){
        this._age = value;
    }

    set salary(value){
        this._salary = value;
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, language){
        super(name, age, salary);
        this._language = language;
    }

    get salary(){
        return this._salary * 3;
    }
}

const programer1 = new Programmer('Olena', 30, 20000, 'html');
const programer2 = new Programmer('Mykola', 40, 40000, 'js');
const programer3 = new Programmer('Oles', 20, 30000, 'php');

console.log(programer1,programer2,programer3)
console.log(programer1.salary, programer2.salary, programer3.salary)
