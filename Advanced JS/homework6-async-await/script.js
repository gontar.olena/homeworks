const API = "https://api.ipify.org/?format=json"
const physicalAddressAPI = "http://ip-api.com/json/"
const lookupButton = document.querySelector("#lookup")
const reply = document.querySelector(".reply")

async function openIP() {
    
    const dataIP = await fetch(API).then(response => response.json());
    return dataIP.ip
    
}//promise


async function physicalAddress(ip) {
    
    const physicalAddress = await fetch(`${physicalAddressAPI}${ip}`).then(response => response.json());
    return physicalAddress
    
}//promise

lookupButton.addEventListener('click', () => {openIP().then((ip)=>{
        reply.innerHTML = `<p><span class="bold"> Your open IP: </span>${ip}</p>`
        physicalAddress(ip)
                .then(({timezone, country, regionName, city, zip}) => {
                    console.log(timezone, country, regionName, city, zip)
                    reply.insertAdjacentHTML('beforeend',`
                    <p><span class="bold"> Your continent: </span>${timezone}</p>
                    <p><span class="bold"> Your country: </span>${country}</p>
                    <p><span class="bold"> Your region: </span>${regionName}</p>
                    <p><span class="bold"> Your city: </span>${city}</p>
                    <p><span class="bold"> Your zip: </span>${zip}</p>
                    `)
                })
               
    })
})


