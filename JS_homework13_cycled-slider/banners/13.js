// ## Теоретичні питання

// 1. Опишіть своїми словами різницю між функціями `setTimeout()` і `setInterval(`)`.
// setTimeout() - виклик функції через деякий час, один раз
// setInterval() - виклик функції з заданою періодичністю
// 2. Що станеться, якщо в функцію `setTimeout()` передати нульову затримку? Чи спрацює вона миттєво і чому?
// Справцює після завершення виконання поточного коду.
// 3. Чому важливо не забувати викликати функцію `clearInterval()`, коли раніше створений цикл запуску вам вже не потрібен?
//Важливо очищати, щоб зупиняти опрацювання, і не виникало ситуацій, коли кілька таймерів працюють одночасно.

// ## Завдання

// Реалізувати програму, яка циклічно показує різні картинки. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - У папці `banners` лежить HTML код та папка з картинками.
// - При запуску програми на екрані має відображатись перша картинка.
// - Через 3 секунди замість неї має бути показано друга картинка.
// - Ще через 3 секунди – третя.
// - Ще через 3 секунди – четверта.
// - Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
// - Після запуску програми десь на екрані має з'явитись кнопка з написом `Припинити`.
// - Після натискання на кнопку `Припинити` цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.
// - Поруч із кнопкою `Припинити` має бути кнопка `Відновити показ`, при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.
// - Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги. 

// #### Необов'язкове завдання підвищеної складності
// - При запуску програми на екрані повинен бути таймер з секундами та мілісекундами, що показує, скільки залишилося до показу наступної картинки.
// - Робити приховування картинки та показ нової картинки поступовим (анімація fadeOut/fadeIn) протягом 0.5 секунди.

// #### Література:
// - [setTimeout и setInterval](https://learn.javascript.ru/settimeout-setinterval)

let imagesToShow = document.querySelectorAll(".image-to-show")
let timerDiv = document.querySelector(".timer")

let timerSeconds = 10;

const imageChange = () => {
    timerDiv.innerText = `${timerSeconds.toFixed(2)} sec`
    timerSeconds = (timerSeconds - 0.01);
    if (timerSeconds < 0) {
    
        let visibleImage = document.querySelector(".visible");
        let currentIndex = 0;
        imagesToShow.forEach((element, index) => {
            if (element === visibleImage){
                currentIndex = index;
                console.log(currentIndex)
                visibleImage.style.transition = "opacity 0.5s";
                visibleImage.style.opacity = 0
                visibleImage.classList.remove('visible')
            }    
        });
    
        imagesToShow[(currentIndex+1)%imagesToShow.length].classList.add('visible')
        imagesToShow[(currentIndex+1)%imagesToShow.length].style.transition = "opacity 0.5s"
        imagesToShow[(currentIndex+1)%imagesToShow.length].style.opacity = 1
        timerSeconds = 10;
    }

}

let timer = setInterval(imageChange, 10);

let stopButton = document.querySelector("#stop")
stopButton.addEventListener('click', ()=>{
    setTimeout(() => { clearInterval(timer); console.log(`stopped`) }, 0)
    resumeButton.disabled = false
})

let resumeButton = document.querySelector("#resume")
resumeButton.addEventListener('click', () => {
    timer = setInterval(imageChange, 10)
    resumeButton.disabled = true
})




  
  


