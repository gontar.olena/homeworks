const services = [
    {
      "title": "web design",
      "img": "./image/services/Layer 9 1.png",
      "text": "Proin dapibus, purus in maximus ultrices, ligula elit tempor metus, in egestas mauris leo et augue. Nulla volutpat dapibus nunc et euismod. Proin gravida orci lectus, nec blandit nisl semper in. Pellentesque molestie suscipit nibh. Mauris imperdiet, elit at iaculis euismod, mi quam porttitor dolor, ut tempus tortor urna eget mauris. Vivamus sodales vel felis id gravida. In vulputate varius diam, non ullamcorper massa blandit eu. Phasellus et dignissim ipsum. Donec purus dui, hendrerit ut mi facilisis, dictum tincidunt est.",

    },
    {
        "title": "graphic design",
        "img": "./image/services/Layer 9 2.png",
        "text": "Suspendisse aliquet magna vel pulvinar dapibus. Aenean viverra, ipsum a pellentesque imperdiet, nisl est vestibulum odio, ac imperdiet eros libero rhoncus libero. Cras semper viverra erat vitae lacinia. Curabitur interdum ex sed elit finibus lobortis. Morbi nec dui lobortis, pellentesque nunc vitae, feugiat tortor. Nam vehicula sodales lorem malesuada elementum. Maecenas ut molestie sapien."
    },
    {
        "title": "online support",
        "img": "./image/services/Layer 9 3.png",
        "text": "Suspendisse orci diam, facilisis ac quam porta, auctor mollis augue. Fusce mi risus, volutpat id blandit sit amet, consectetur in urna. Praesent mattis at turpis nec semper. Donec iaculis orci at sapien interdum, a lacinia justo aliquam. Fusce tincidunt auctor ultrices. Vivamus sed lectus mollis, imperdiet eros at, facilisis nisl."
    },
    {
        "title": "app design",
        "img": "./image/services/Layer 9 4.png",
        "text": "Mauris faucibus erat eget enim posuere, ac vestibulum eros viverra. Pellentesque iaculis blandit sem. Nunc porttitor eros a nunc tincidunt, pharetra consectetur magna dapibus. Maecenas sagittis sapien eu orci scelerisque scelerisque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi hendrerit sit amet augue non interdum"
    },
    {
        "title": "online marketing",
        "img": "./image/services/Layer 9 5.png",
        "text": "Aliquam sodales nulla a turpis convallis, nec auctor mauris semper. Nulla eget magna rhoncus, vestibulum quam vitae, dictum purus. Sed nibh felis, faucibus sed rutrum id, ultricies vitae mauris. Donec rutrum lacus eget tincidunt vulputate. Duis lobortis diam augue, a tempor leo rhoncus at"
    },
    {
        "title": "seo service",
        "img": "./image/services/Layer 9 6.png",
        "text": " Ut sollicitudin eleifend libero, non pulvinar sapien hendrerit id. Fusce sit amet maximus massa. Suspendisse vitae finibus ipsum. Fusce vel ipsum sagittis, faucibus urna at, ornare nulla."
    }
]

const works = [
                { "work":"graphic-design",
                  "img":"./image/graphic\ design/graphic-design5.jpg",
                  "title": "Graphic design"  
                },
                { "work":"graphic-design",
                  "img":"./image/graphic\ design/graphic-design6.jpg",
                  "title": "Graphic design"   
                },
                { "work":"graphic-design",
                  "img":"./image/graphic\ design/graphic-design7.jpg",
                  "title": "Graphic design"  
                },
                { "work":"graphic-design",
                  "img":"./image/graphic\ design/graphic-design8.jpg",  
                  "title": "Graphic design"  
                },
                { "work":"web-design",
                  "img":"./image/web\ design/web-design5.jpg" ,
                  "title": "Web design"   
                },
                { "work":"web-design",
                  "img":"./image/web\ design/web-design6.jpg"  ,
                  "title": "Web design"   
                },
                { "work":"web-design",
                  "img":"./image/web\ design/web-design7.jpg"  ,
                  "title": "Web design"   
                },
                { "work":"web-design",
                   "img":"./image/web\ design/web-design1.jpg" ,
                   "title": "Web design"    
                },
                { "work":"landing-pages",
                  "img":"./image/landing\ page/landing-page5.jpg"  ,
                  "title": "Landing pages"   
                },
                { "work":"landing-pages",
                  "img":"./image/landing\ page/landing-page6.jpg"  ,
                  "title": "Landing pages"   
                },
                { "work":"landing-pages",
                  "img":"./image/landing\ page/landing-page7.jpg" ,
                  "title": "Landing pages"    
                },
                { "work":"landing-pages",
                  "img":"./image/landing\ page/landing-page1.jpg",
                  "title": "Landing pages"     
                },
                { "work":"wordpress",
                "img":"./image/wordpress/wordpress5.jpg"  ,
                "title": "Wordpress"   
                },
                { "work":"wordpress",
                "img":"./image/wordpress/wordpress6.jpg"  ,
                "title": "Wordpress"    
                },
                { "work":"wordpress",
                "img":"./image/wordpress/wordpress7.jpg"  ,
                "title": "Wordpress"    
                },
                { "work":"wordpress",
                "img":"./image/wordpress/wordpress8.jpg"   ,
                "title": "Wordpress"   
                },
                { "work":"landing-pages",
                "img":"./image/landing page/landing-page1.jpg"  ,
                "title": "Landing pages"    
                },
                { "work":"landing-pages",
                    "img":"./image/landing page/landing-page2.jpg"  ,
                    "title": "Landing pages"     
                },
                { "work":"landing-pages",
                    "img":"./image/landing page/landing-page3.jpg" ,
                    "title": "Landing pages"      
                },
                { "work":"landing-pages",
                    "img":"./image/landing page/landing-page4.jpg"   ,
                    "title": "Landing pages"    
                },
                { "work":"wordpress",
                "img":"./image/wordpress/wordpress1.jpg"   ,
                "title": "Wordpress"    
                },
                { "work":"wordpress",
                "img":"./image/wordpress/wordpress2.jpg"  ,
                "title": "Wordpress"    
                },
                { "work":"wordpress",
                "img":"./image/wordpress/wordpress3.jpg"  ,
                "title": "Wordpress"    
                },
                { "work":"wordpress",
                "img":"./image/wordpress/wordpress4.jpg"  ,
                "title": "Wordpress"    
                }
            ]

const people = [
  {src:"./image/people/boy1.png",
   name: "Hasan Ali",
   title: "UX Designer"
  },
  {src:"./image/people/boy2.png",
   name: "Massi Ma",
   title: "Web Designer"
  },
  {src:"./image/people/girl1.png",
   name: "Soni Alina",
   title: "Developer"
  },
  {src:"./image/people/girl2.png",
  name: "Johnson Kate",
  title: "Marketing Advisor"
 }    
]

const gallery = [
  {image:"./image/gallery/80493541_1281644acb_o\ 1.png",
   sizeclass:"grid-item--width2"},
  {image:"./image/gallery/7328272788_c5048326de_o\ 1.png",
  sizeclass:"grid-item--width2"},
  {image:"./image/gallery/Brazil-staduims-IA-1\ 1.png",
  sizeclass:"grid-item--width2"},
  {image:"./image/gallery/IA-billionares-6\ 1.png",
  sizeclass:"grid-item--width2"},
  {image:"./image/gallery/ia-Pools-1\ 1.png",
  sizeclass:""},
  {image:"./image/gallery/ringve-museum-1\ 1.png",
  sizeclass:"grid-item--width2"},
  {image:"./image/gallery/vanglo-house-1\ 1.png",
  sizeclass:""},
  {image:"./image/gallery/vanglo-house-4\ 1.png",
  sizeclass:"grid-item--width2"},
  {image:"./image/gallery/vanglo-house-6\ 1.png",
  sizeclass:"grid-item--width3"}
]

let $grid = $('.gallery .grid').masonry({
    // options
    itemSelector: '.gallery .grid-item',
    columnWidth: 120,
    gutter: 20
});

$grid.imagesLoaded().progress( function() {
  $grid.masonry();
});

const loaderOn = (section) => {
  
  let myButton = document.querySelector( "." + section + " .button-load-more")
  let mySpinner = document.querySelector( "." + section + " .spinner")

  myButton.classList.add("invisible")
  mySpinner.classList.remove("invisible")
}

const loaderOff = (section) => {
 
  let myButton = document.querySelector( "." + section + " .button-load-more")
  let mySpinner = document.querySelector( "." + section + " .spinner")
 
  myButton.classList.remove("invisible")
  mySpinner.classList.add("invisible")
}

  //Our services
  let servicesCollection = document.querySelectorAll(".service-list .list-item")
  let servicesCollectionWrapper = document.querySelector(".service-list")
  let serviceImage = document.querySelector(".services .service-image")
  let serviceText = document.querySelector(".services .service-text")
  servicesCollectionWrapper.addEventListener("click", (event)=>{
    servicesCollection.forEach(element => {
        if (element!==event.target){
            element.classList.remove('active')
        }
        if (element===event.target){
            element.classList.add('active')        
        }
        services.forEach(item => {
            if (item.title === event.target.innerText.toLowerCase()){
                console.log(event.target.innerText.toLowerCase())
                serviceImage.setAttribute("src", item.img)
                serviceText.innerText = item.text
            }
        });
        
    });
  })

  //Our work
  let worksCollection = document.querySelectorAll(".work .work-list .list-item")
  let worksCollectionWrapper = document.querySelector(".work .work-list")
  
  worksCollectionWrapper.addEventListener("click", (event)=>{
    worksCollection.forEach(element => {
      element.classList.remove("active")    
    });
    event.target.classList.add("active")
    let worksCollectionImages = document.querySelectorAll(".image-wrapper .item-wrapper")
    let filterClass = event.target.innerText.toLowerCase().replace(" ","-")
 
    worksCollectionImages.forEach(element => {
      
       if (!element.classList.contains(filterClass)) {
        element.classList.add("invisible")
       } 
       else {
        element.classList.remove("invisible")
       }
    });
    if (filterClass === "all"){
        worksCollectionImages.forEach(element => {
            element.classList.remove("invisible")
        })
    }
  })

  //Our Work Load More button 

let workButton = document.querySelector("#workbutton")
let workImageWrapper = document.querySelector(".work .image-wrapper")


workButton.addEventListener("click",()=>{
    loaderOn("work")
    setTimeout(() => {
                        //find active filter
                        worksCollection.forEach(element => {
                        
                          if (element.classList.contains("active")){
                            myfilterClass = element.innerText.toLowerCase().replace(" ","-")
                           }
                        });
                        
                        if (typeof myfilterClass === 'undefined'){
                          myfilterClass="all"
                        };
                        //turn off the loader
                        loaderOff("work")
                        //add works
                        if (sessionStorage.workclicks === "1"){      
                            workButton.classList.add("invisible")
                            sessionStorage.workclicks = 0;
                        
                            for (let index = 12; index < 24; index++) {
                                let element = works[index]; 
                                
                                if ((element.work === myfilterClass)||(myfilterClass==='all')) {
                                  workImageWrapper.insertAdjacentHTML("beforeend",`<div class="item-wrapper ${element.work}"> <img class="image" src="${element.img}" alt="work example"> <div class="item-switcher"> <svg class="item-switcher-icon"><use xlink:href="#icon"></use></svg><p class="text1 colored-green">${element.title}</p><p class="text2">web design</p></div></div>`)
                                }
                                else {
                                 workImageWrapper.insertAdjacentHTML("beforeend",`<div class="item-wrapper ${element.work} invisible"> <img class="image" src="${element.img}" alt="work example"> <div class="item-switcher"> <svg class="item-switcher-icon"><use xlink:href="#icon"></use></svg><p class="text1 colored-green">${element.title}</p><p class="text2">web design</p></div></div>`)
                                }
                            }
                            
                        }

                       else {
                            sessionStorage.workclicks = 1;
                        
                            for (let index = 0; index < 12; index++) {
                              let element = works[index]; 
                                                
                              if  ((element.work === myfilterClass)||(myfilterClass==='all'))  {
                                workImageWrapper.insertAdjacentHTML("beforeend",`<div class="item-wrapper ${element.work}"> <img class="image" src="${element.img}" alt="work example"> <div class="item-switcher"> <svg class="item-switcher-icon"><use xlink:href="#icon"></use></svg><p class="text1 colored-green">${element.title}</p><p class="text2">web design</p></div></div>`)
                                
                              }
                              else {
                               workImageWrapper.insertAdjacentHTML("beforeend",`<div class="item-wrapper ${element.work} invisible"> <img class="image" src="${element.img}" alt="work example"> <div class="item-switcher"> <svg class="item-switcher-icon"><use xlink:href="#icon"></use></svg><p class="text1 colored-green">${element.title}</p><p class="text2">web design</p></div></div>`)
                              } 
                            }
                        }
    }, 2000) 
   
})

//People Carusel
let peopleWrapper = document.querySelector(".people .small-image-wrapper")
let peoplePhotos = document.querySelectorAll(".people .small-people-photo")
peopleWrapper.addEventListener("click",(event) => {
    
    if (event.target.classList.contains("small-people-photo")){
       document.querySelector(".people .people-photo").setAttribute("src",event.target.getAttribute("src"))
       people.forEach(element => {
        if (element.src === event.target.getAttribute("src") ) {
          document.querySelector(".people .people-name").innerText = element.name
          document.querySelector(".people .people-job-title").innerText = element.title
        }
        
       });
    }
    peoplePhotos.forEach(element => {
      if (element.getAttribute("src") === event.target.getAttribute("src"))
        {
        element.classList.add("active")     
        } 
    else {
        element.classList.remove("active")
    }
  }) 
})
//left arrow
let leftArrow = document.querySelector("#leftarrow")
let leftKey = 0
leftArrow.addEventListener("click", () => {
  peoplePhotos.forEach((element,key) => {
    if (element.classList.contains("active"))
      {
      element.classList.remove("active") 
        if (key === 0){
          leftKey = peoplePhotos.length-1
        }   
        else {
          leftKey = key-1
        }
      } 

  })
      console.log(leftKey)
      peoplePhotos[leftKey].classList.add("active")
      document.querySelector(".people .people-photo").setAttribute("src",peoplePhotos[leftKey].getAttribute("src"))   
      people.forEach(element => {
        if (element.src ===  document.querySelector(".people .people-photo").getAttribute("src") ) {
          document.querySelector(".people .people-name").innerText = element.name
          document.querySelector(".people .people-job-title").innerText = element.title
        }
      })
}) 

//right arrow
let rightArrow = document.querySelector("#rightarrow")
let rightKey = 0
rightArrow.addEventListener("click", () => {
  peoplePhotos.forEach((element,key) => {
    if (element.classList.contains("active"))
      {
      element.classList.remove("active") 
      if (key === peoplePhotos.length-1){
        rightKey = 0
      }   
      else {
        rightKey = key+1
      }
    } 
   
  })
      console.log(rightKey)
      peoplePhotos[rightKey].classList.add("active")
      document.querySelector(".people .people-photo").setAttribute("src",peoplePhotos[rightKey].getAttribute("src"))   
      people.forEach(element => {
        if (element.src ===  document.querySelector(".people .people-photo").getAttribute("src") ) {
          document.querySelector(".people .people-name").innerText = element.name
          document.querySelector(".people .people-job-title").innerText = element.title
        }
      })
}) 


 //Gallery button 

 let galleryButton = document.querySelector("#gallerybutton")
 let galleryImageWrapper = document.querySelector(".gallery .grid")
 // let workItemsCollection = document.querySelectorAll(".work .image-wrapper .item-wrapper")
 galleryButton.addEventListener("click",()=>{
     loaderOn("gallery")
     setTimeout(() => {loaderOff("gallery")
          
          
          for (let index = 0; index < gallery.length; index++) {
                  const element = gallery[index];       
                  console.log(element.image)
                  let item = `<div class="grid-item ${element.sizeclass}"><img src="${element.image}" alt="gallery" class="grid-image"/></div>`
                  let $item = $(item)
                  $grid.append($item).masonry( 'appended', $item)
                }
         
              
            galleryButton.classList.add("invisible")                                                                       
     }, 2000) 
    
 })







