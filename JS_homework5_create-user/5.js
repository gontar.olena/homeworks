// ## Теоретичні питання
// 1. Опишіть своїми словами, що таке метод об'єкту
//      Метод об'єкту, це властивість об'єкту, значенням якої є функція, що працює зі значеннями інших властивостей об'єкта.

// 2. Який тип даних може мати значення властивості об'єкта?
//      Значення властивості об'єкту може мати або примітивний, або складний тип даних, включаючи інші об'єкти чи функції

// 3. Об'єкт це посилальний тип даних. Що означає це поняття?
//      Це означає, що в змінній зберігається посилання на об'єкт, і при копіюванні буде копіюватися не сам об'єкт, а посилання на нього.

// ## Завдання

// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Написати функцію `createNewUser()`, яка буде створювати та повертати об'єкт `newUser`.
// - При виклику функція повинна запитати ім'я та прізвище.
// - Використовуючи дані, введені юзером, створити об'єкт `newUser` з властивостями `firstName` та `lastName`.
// - Додати в об'єкт `newUser` метод `getLogin()`, який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, `Ivan Kravchenko → ikravchenko`).
// - Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію `getLogin()`. Вивести у консоль результат виконання функції.

// #### Необов'язкове завдання підвищеної складності

// - Зробити так, щоб властивості `firstName` та `lastName` не можна було змінювати напряму. Створити функції-сеттери `setFirstName()` та `setLastName()`, які дозволять змінити дані властивості.

// #### Література:

// - [Об'єкти як асоціативні масиви](https://learn.javascript.ru/object)
// - [Object.defineProperty()](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty)

const createNewUser = () => {
    firstName = prompt("Enter your first name: ")
    lastName = prompt("Enter your last name: ")
    const newUser = {
            firstName:firstName,
            lastName:lastName,
            getLogin(){
            return (this.firstName[0] + this.lastName).toLowerCase()
            },  
        
            setFirstName(value){
                Object.defineProperty(this,"firstName",{writable:true})
                this.firstName = value
                Object.defineProperty(this,"firstName",{writable:false})
             },

            setLastName(value){
                Object.defineProperty(this,"lastName",{writable:true})
                this.lastName = value
                Object.defineProperty(this,"lastName",{writable:false})
            }
            
    }
    Object.defineProperty(newUser,"firstName",{writable:false})
    Object.defineProperty(newUser,"lastName",{writable:false})
    return newUser
}

newUser = createNewUser()
console.log(newUser.getLogin())
// Changing the user via private function-setters
newUser.setFirstName("Mykola")
newUser.setLastName("Slobodian")
console.log(newUser.getLogin())


