let menu_button = document.querySelector(".icon-wrapper");
let menu = document.querySelector(".header-menu");
let menu_button_pull = document.querySelector(".svg-menu-button_pull");
let menu_button_close = document.querySelector(".svg-menu-button_close");
menu_button.addEventListener("click", (event)=>{
    console.log(event.target.getAttribute("xlink:href"))
    if (event.target.getAttribute("xlink:href") === "#svg-menu-button_pull") {
        menu.style.display = "block";
        menu_button_close.style.display = "block";
        menu_button_pull.style.display = "none";
    }
    if (event.target.getAttribute("xlink:href") === "#svg-menu-button_close") {
        menu.style.display = "none";
        menu_button_close.style.display = "none";
        menu_button_pull.style.display = "block";
    }
})

